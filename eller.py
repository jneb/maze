#!python3
"""Maze generator using Eller's algorithm.
This algorithm constructs the maze during printing, from top to bottom!
"""
import sys
from random import randrange, random

#parameters
#probability of vertical wall (if there is a choice)
VERTPROB = .2
#probability of horizontal wall
HORPROB = .5

SPACE = ' '*2
VERTLINE = '\N{box drawings light vertical}'
HORLINE = '\N{box drawings light horizontal}'*2
CROSSING = '\N{box drawings light vertical and horizontal}'
LEFTBORDER = '\N{box drawings light vertical and right}'
RIGHTBORDER = '\N{box drawings light vertical and left}'
LOWERBORDER = '\N{box drawings light up and horizontal}'
UPPERBORDER = '\N{box drawings light down and horizontal}'
TOPLEFT = '\N{box drawings light down and right}'
TOPRIGHT = '\N{box drawings light down and left}'
BOTLEFT = '\N{box drawings light up and right}'
BOTRIGHT = '\N{box drawings light up and left}'

class LineStatus:
    """Keep information about the paths to the current line.
    Data structure:
        self.s is a dictionary of lists.
        Each list contains all cell indices that are connected
        via an existing passage
        We make sure the connected lists are identical
    """
    def __init__(self, width):
        self.width = width
        self.s = {i:[i] for i in range(width)}

    def connected(self, i, j):
        """Indicate if cell i and j are connected."""
        return self.s[i] is self.s[j]

    def connectRight(self, i):
        """Connect cell i with cell i+1"""
        si, sj = self.s[i], self.s[i+1]
        #optimize for loop so sj is the shortest
        if len(si)<len(sj): si, sj = sj, si
        #set all cells in the right set identical to the left set
        for j in sj: self.s[j] = si
        #extend the left set (updating all sets)
        si.extend(sj)

    def horWall(self, i):
        """Put horizontal wall under cell."""
        #decouple cell from passage
        self.s[i].remove(i)
        #make new passage
        self.s[i] = [i]

    def printBorder(self, bottom):
        """Print first line"""
        opening = randrange(self.width)
        border = LOWERBORDER if bottom else UPPERBORDER
        print((BOTLEFT if bottom else TOPLEFT),
            border.join((HORLINE, SPACE)[i==opening]
                for i in range(self.width)),
            (BOTRIGHT if bottom else TOPRIGHT),
            sep='')

    def printVertical(self, prob=VERTPROB):
        """Determine vertical edges, update and print"""
        #left edge
        print(VERTLINE, end='')
        for i in range(self.width-1):
            #connect cells horizontally: only allowed if not already connected
            if self.connected(i, i+1) or random()<prob:
                print(SPACE, VERTLINE, sep='', end='')
            else:
                self.connectRight(i)
                print(SPACE, ' ', sep='', end='')
        #right edge
        print(SPACE, VERTLINE, sep='')

    def printHorizontal(self):
        """Determine horizontal edges, update and print"""
        #make "vertical connection" flags for all sets
        border = LEFTBORDER
        for i in range(self.width):
            #make connection if last chance, or if we like to
            if len(self.s[i])==1 or random()>HORPROB:
                #make vertical connection, cell remains the same
                print(border, SPACE, sep='', end='')
            else:
                #make wall, update cell
                self.horWall(i)
                print(border, HORLINE, sep='', end='')
            border = CROSSING
        print(RIGHTBORDER)


    def __repr__(self):
        return 'line({})'.format(SPACE.join(str(s[0]) for s in self.s.values()))

def maze(width, height):
    #start
    line = LineStatus(width)
    line.printBorder(False)
    #maze
    for i in range(height):
        line.printVertical()
        line.printHorizontal()
    #connect all islands
    line.printVertical(prob=0)
    line.printBorder(True)


if __name__ == '__main__':
    if len(sys.argv)<3: sys.argv.extend(('29', '15'))
    maze(int(sys.argv[1]), int(sys.argv[2]))
