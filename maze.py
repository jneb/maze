#!python3
"""Draw a maze using utf symbols in the current DOS window.
"""
import curses
from array import array
from itertools import product
from random import choice, randrange, random
from sys import argv
from math import atan2, pi
import time

CELLHEIGHT, CELLWIDTH = 2, 3


def getWindowSize():
    """Return window height and width of the DOS box we are in
    warn is ignored, this routine is instant
    """
    # get window size out of the guts of Windows kernel
    from ctypes import windll, create_string_buffer
    from struct import unpack
    STDINHANDLE, STDOUTHANDLE, STDERRHANDLE = -10, -11, -12
    h = windll.kernel32.GetStdHandle(STDOUTHANDLE)
    csbi = create_string_buffer(22)
    res = windll.kernel32.GetConsoleScreenBufferInfo(h, csbi)
    if not res:
        raise ValueError("Can't figure out window size")
    if res:
        (bufx, bufy, curx, cury, wattr,
         left, top, right, bottom, maxx, maxy) = unpack(
            "hhhhHhhhhhh", csbi.raw)
        windowHeight, windowWidth = bottom - top + 1, right - left + 1
    return windowHeight, windowWidth


class XYarray:
    """Simple two dimensional array.
    """

    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.data = array('B', b'\x00' * (height * width))

    def __repr__(self):
        return 'XYarray({0.width}, {0.height})'.format(self)

    def __getitem__(self, item):
        x, y = item
        width, height = self.width, self.height
        if x < 0:
            x += width
        if y < 0:
            y += height
        if not(0 <= x <= width and 0 <= y < height):
            raise ValueError("x,y not within bounds")
        return self.data[x + width * y]

    def __setitem__(self, item, value):
        x, y = item
        width, height = self.width, self.height
        if x < 0:
            x += width
        if y < 0:
            y += height
        if not(0 <= x <= width and 0 <= y < height):
            raise ValueError("x,y not within bounds")
        self.data[x + self.width * y] = value


class Maze:
    """Describe a maze.
    There is a virtual "bottom" and "right" array containing wall booleans.
    """

    def __init__(self, cols, rows):
        self.cols, self.rows = cols, rows
        self.bottom = XYarray(cols, rows)
        self.right = XYarray(cols, rows)
        self.bottom.data[:] = self.right.data[:] = array(
            'B', b'\x01' * rows * cols)
        # make entrance and exit
        self.entrance = randrange(cols)
        self.bottom[randrange(cols), rows - 1] = False

    def __repr__(self):
        return 'Maze({0.cols},{0.rows})'.format(self)

    def drawAll(self, win):
        """Draw entire maze in curses window"""
        for x, y in product(range(self.cols), range(self.rows)):
            self.drawCell(win, x, y)

    def drawCell(self, win, x, y):
        """Draw maze cell at x,y"""
        posy, posx = y * CELLHEIGHT, x * CELLWIDTH
        win.addch(posy, posx, self.getCorner(x, y))
        if self.getHorWall(x, y):
            for i in range(1, CELLWIDTH):
                win.addch(curses.ACS_BSBS)
        else:
            win.addstr(' ' * (CELLWIDTH - 1))
        win.addch(self.getCorner(x + 1, y))

        for i in range(1, CELLHEIGHT):
            if self.getVertWall(x, y):
                win.addch(posy + i, posx, curses.ACS_SBSB)
            else:
                win.addch(posy + i, posx, ' ')
            win.addstr(' ' * (CELLWIDTH - 1))
            if self.right[x, y]:
                win.addch(curses.ACS_SBSB)
            else:
                win.addch(' ')

        win.addch(posy + CELLHEIGHT, posx, self.getCorner(x, y + 1))
        if self.bottom[x, y]:
            for i in range(1, CELLWIDTH):
                win.addch(curses.ACS_BSBS)
        else:
            win.addstr(' ' * (CELLWIDTH - 1))
        try:
            win.addch(self.getCorner(x + 1, y + 1))
        except curses.error:
            # bottom right of screen gives error, no problem
            if x == self.cols - 1 and y == self.rows - 1:
                pass
            else:
                raise

    def getHorWall(self, x, y):
        """Determine if there is a wall between x,y-1 and x,y"""
        if not 0 <= x < self.cols:
            return False
        if y:
            return self.bottom[x, y - 1]
        return x != self.entrance

    def getVertWall(self, x, y):
        """Determine if there is a wall between x-1,y and x,y"""
        if not 0 <= y < self.rows:
            return False
        if x:
            return self.right[x - 1, y]
        return True

    @classmethod
    def initCornerChars(cls):
        cls.cornerChars = {
            0: ' ',
            1: ' ',
            2: ' ',
            3: curses.ACS_BBSS,
            4: ' ',
            5: curses.ACS_BSBS,
            6: curses.ACS_BSSB,
            7: curses.ACS_BSSS,
            8: ' ',
            9: curses.ACS_SBBS,
            10: curses.ACS_SBSB,
            11: curses.ACS_SBSS,
            12: curses.ACS_SSBB,
            13: curses.ACS_SSBS,
            14: curses.ACS_SSSB,
            15: curses.ACS_SSSS,
        }

    def getCorner(self, x, y):
        """Determine character needed to left top corner of cell (x,y)"""
        top = self.getVertWall(x, y - 1)
        right = self.getHorWall(x, y)
        bottom = self.getVertWall(x, y)
        left = self.getHorWall(x - 1, y)
        return self.cornerChars[top << 3 |
                                right << 2 | bottom << 1 | left << 0]

    def makeMaze(self, straightProb=.7, callback=None):
        """Make a maze and draw while generating.
        straightProb is the probability a path continues as straight as possible.
        Determine the maze style:
        <.3: treelike maze growing from entry
        .3-.6: twisty
        .6-.8: regular
        .8-.9: straight
        >.9: boring, lots of spirals
        """
        cols, rows = self.cols, self.rows
        # list of points that possibly allow further adding of branches
        # array of cells that are in use
        self.used = XYarray(cols, rows)
        # invalid direction
        direction = '*'
        x, y = self.entrance, 0
        self.used[x, y] = True
        branchPoints = [(x, y)]
        if callback is not None:
            callback(x, y)
        while branchPoints:
            # compute directions from current starting point
            directions = self.unusedDirections(x, y)
            # generate new starting point if needed
            if (x, y) not in branchPoints \
                    or not directions \
                    or random() > straightProb:
                # we can't continue: find a new random starting point
                x, y = branchPoints.pop(randrange(len(branchPoints)))
                directions = self.unusedDirections(x, y)
                if not directions:
                    # near the end, many branch points are choked by near points
                    # we'll have to find a new one
                    continue
                if len(directions) == 1:
                    # there's only one exit
                    # and we don't have to choose direction
                    direction = directions
                else:
                    # this stays a branchpoint when we are done, for a while
                    branchPoints.append((x, y))
                    direction = choice(directions)
            else:
                # continue from the same starting point; try same direction
                if direction not in directions:
                    # we can't continue straight; find new direction
                    direction = choice(directions)
            # go in chosen direction
            if direction == 'T':
                y -= 1
                self.bottom[x, y] = False
            elif direction == 'L':
                x -= 1
                self.right[x, y] = False
            elif direction == 'B':
                self.bottom[x, y] = False
                y += 1
            else:
                self.right[x, y] = False
                x += 1
            # update new cell
            self.used[x, y] = True
            branchPoints.append((x, y))
            if callback is not None:
                callback(x, y)
        del self.used

    def unusedDirections(self, x, y):
        """Find directions that lead to unused cells
        """
        used = self.used
        result = ''
        if x and not used[x - 1, y]:
            result += 'L'
        if y and not used[x, y - 1]:
            result += 'T'
        if x < self.cols - 1 and not used[x + 1, y]:
            result += 'R'
        if y < self.rows - 1 and not used[x, y + 1]:
            result += 'B'
        return result


class MazeWalker(Maze):
    """Someone walking in a maze, with direction information.
    Direction: 0=right, 1=up, 2=left, 3=down
    Visited: if nonzero, special lines are drawn;
    set to direction where we originally came from (for backtrack)
    """
    # TODO: show solution when done

    def __init__(self, maze, win):
        self.maze = maze
        self.x, self.y = maze.entrance, 0
        self.win = win
        self.direction = 2
        # list of visited cells
        self.visited = XYarray(maze.cols, maze.rows)
        # draw starting point
        self.drawInCell('+')
        self.visited[self.x, self.y] = True

    def __repr__(self):
        return 'MazeWalker({0.maze},win) at pos ({0.x},{0.y}) facing {1}'.format(
            self,
            ['right', 'up', 'left', 'down'][self.direction])

    def drawInCell(self, char=' ', direction=None):
        """Draw in cell.
        If direction is given, draw in wall between cells.
        Default char is space, - or | perpendicular to wall.
        """
        posy, posx = self.y * CELLHEIGHT, self.x * CELLWIDTH
        if direction is None:
            posx += 1
            posy += 1
            char *= CELLWIDTH - 1
        elif direction == 0:
            posx += CELLWIDTH
            posy += CELLHEIGHT >> 1
            if char == ' ':
                char = '-'
        elif direction == 1:
            posx += CELLWIDTH >> 1
            posy += CELLHEIGHT
            if char == ' ':
                char = '|'
        elif direction == 2:
            posy += CELLHEIGHT >> 1
            if char == ' ':
                char = '-'
        elif direction == 3:
            posx += CELLWIDTH >> 1
            if char == ' ':
                char = '|'
        try:
            self.win.addstr(posy, posx, char)
        except curses.error:
            raise TypeError(str(locals()) + '\n' + str(vars(self)))

    def peek(self, direction):
        """See if we can go in that direction;
        return x,y if possible, None otherwise.
        """
        maze = self.maze
        x, y = self.x, self.y
        # step if possible
        if direction == 0 and not maze.right[x, y]:
            return x + 1, y
        elif direction == 1 and not maze.bottom[x, y]:
            return x, y + 1
        elif direction == 2 and x > 0 and not maze.right[x - 1, y]:
            return x - 1, y
        elif direction == 3 and y > 0 and not maze.bottom[x, y - 1]:
            return x, y - 1

    def step(self, direction):
        """Step in the given direction and draw trace.
        Raises ValueError if you hit a wall.
        """
        maze = self.maze
        newxy = self.peek(direction)
        if newxy is None:
            raise ValueError("Can't go that way")
        if self.visited[newxy]:
            # old path
            chars = '=!=!*'
        else:
            chars = '-|-|.'
        # draw step
        self.drawInCell(char=chars[direction], direction=direction)
        self.x, self.y = newxy
        # mark as visited (and remember where we came from originally)
        self.drawInCell(chars[4])
        if not self.visited[self.x, self.y]:
            self.visited[self.x, self.y] = direction | 4
        # remember last direction
        self.direction = direction

    def solved(self):
        """Determine if we are there.
        """
        x, y = self.x, self.y
        return y == self.maze.rows - 1 and not self.maze.bottom[x, y]

    def findExit(self):
        """Search the exit and give coordinates of the cell next to it.
        """
        y = self.maze.rows - 1
        for x in range(self.maze.cols):
            if not self.maze.bottom[x, y]:
                return x, y

    def runSolver(self):
        """Run a solver to find the exit.
        Call decideDirection() to find next direction to go.
        decideDirection does right hand search and can be overridden.
        """
        win = self.win
        win.attrset(0)
        self.drawInCell(direction=3)
        while not self.solved() and win.getch() == -1:
            # determine which way to go
            self.doStep()
            # draw
            win.refresh()
            curses.napms(10)
        # ready: draw exit line
        self.drawInCell(direction=1)
        win.refresh()

    def doStep(self):
        """The right hand solve method.
        """
        absoluteDir = self.direction
        # try right, straight, left, back
        for relativeDir in (1, 0, -1, 2):
            try:
                self.step(absoluteDir + relativeDir & 3)
                break
            except ValueError:
                pass
        else:
            raise ValueError("Stuck in maze!")


class TreeMazeWalker(MazeWalker):
    """Go straight if possible, otherwise right.
    Is more complicated when backtracking, but may be more efficient.
    """

    def __init__(self, maze, win):
        super().__init__(maze, win)
        self.direction = 1

    def doStep(self):
        absoluteDir = self.direction
        # try right, straight, left, back if not yet visited
        for relativeDir in (0, 1, -1, 2):
            d = absoluteDir + relativeDir & 3
            # see if we can go where we haven't been
            newxy = self.peek(d)
            if not newxy or self.visited[newxy]:
                continue
            # go
            self.step(d)
            return
        else:
            # we can't go to a new place; go back to where we came from
            self.step(2 + self.visited[self.x, self.y] & 3)


class DirectedSolver(MazeWalker):
    """Prefer the direction that points towards the exit.
    """

    def __init__(self, maze, win):
        super().__init__(maze, win)
        self.direction = 1
        self.exitX, self.exitY = self.findExit()

    def doStep(self):
        # try right, straight, left, back if not yet visited
        # get distances to exit as a regular vector dX,dY
        dX, dY = self.x - self.exitX, self.exitY - self.y
        # figure out octant
        # TODO finish this
        if abs(self.x - self.exitX) < self.exitY - self.y:
            direction = ''
        for relativeDir in (dir1, dir2, 0, 1, -1, 2):
            d = absoluteDir + relativeDir & 3
            # see if we can go where we haven't been
            newxy = self.peek(d)
            if not newxy or self.visited[newxy]:
                continue
            # go
            self.step(d)
            return
        else:
            # we can't go to a new place; go back to where we came from
            self.step(2 + self.visited[self.x, self.y] & 3)

    def directionTo(self, x1, y1, x2, y2):
        """Compute the best two directions from (x1,y1) to (x2,y2)"""
        angle = atan2(y1 - y2, x2 - x1)
        best = round(angle / pi * 2)
        secondBest = best + (1 if angle > best else -1)
        return int(best) & 3, int(secondBest) & 3


def drawMaze(win, drawTime=60):
    """Generate and draw a maze in real time.
    drawTime is the drawing time in seconds
    """
    # determine width and height in cells
    rows, cols = win.getmaxyx()
    height = (rows - 1) // CELLHEIGHT
    width = (cols - 1) // CELLWIDTH
    win.resize(height * CELLHEIGHT + 1, width * CELLWIDTH + 1)
    maze = Maze(width, height)
    # slow down drawing to make it interesting to watch
    drawCounter = 1
    startTime = time.monotonic()

    def drawCell(x, y):
        nonlocal drawCounter
        maze.drawCell(win, x, y)
        drawCounter += 1
        delay = (drawCounter / (rows * cols) * drawTime) - \
            (time.monotonic() - startTime)
        # if we are ahead of schedule, draw and delay
        if delay > 0:
            curses.napms(int(1000 * delay))
            win.refresh()
    s = .7
    if len(argv) > 1:
        s = float(argv[1]) / 10
    maze.makeMaze(straightProb=s, callback=drawCell)
    return maze


def main(win):
    Maze.initCornerChars()
    win.attrset(curses.A_BOLD)
    win.nodelay(1)
    #win.bkgd(' ', curses.COLOR_BLUE)
    # draw maze in 20 seconds
    maze = drawMaze(win, 20)
    # wait for key
    while win.getch() == -1:
        curses.napms(100)
    TreeMazeWalker(maze, win).runSolver()
    while win.getch() == -1:
        curses.napms(100)


if __name__ == '__main__':
    curses.wrapper(main)