# README #

Two maze generator programs, as it the world didn't have enough yet :-)

## How do I get set up? ###

### maze.py: written for Windows with curses.
The twistyness of the maze is selectable from 1 (very twisty) to 9 (almost spiral)
The drawing is delayed so that is takes about 20 seconds.

### eller.py: generate a maze while printing
The parameters are the width and heigt of the maze (in cells).
The entry is at the top and the exit at the bottom.
Due to the way the maze is generated, there is not limit to the height:
memory usage of the program depends on the width only.

## Contribution guidelines ###

* Feel free to contact me at jnebos at google's mail
